#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <list>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <sstream>
#include "cor.hpp"

using namespace std;

class Imagem {
	private:
		string diretorio;
		string formato;
		int altura;
		int largura;
		int maxEscala;
		int fimCabecalho;
		list<Cor> cores;
	public:
		Imagem();
		Imagem(string formato, int altura, int largura, int maxEscala, list<Cor> cores);
		void setDiretorio(string diretorio);
		void setFormato(string formato);
		void setAltura(int altura);
		void setLargura(int largura);
		void setMaxEscala(int maxEscala);
		void setFimCabecalho(int fimCabecalho);
		void setCores(list<Cor> cores);
		string getDiretorio();
		string getFormato();
		int getAltura();
		int getLargura();
		int getMaxEscala();
		int getFimCabecalho();
		list<Cor> getCores();
		void lerImagem(ifstream *arquivo);
		void gravarImagem(ofstream *novoArquivo, ifstream *arquivo);
		void ignorarComentario(ifstream *arquivo);
};
#endif
