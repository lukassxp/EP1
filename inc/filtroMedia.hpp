#ifndef FILTROMEDIA_HPP
#define FILTROMEDIA_HPP

#include "imagem.hpp"
#include "filtro.hpp"
#include "cor.hpp"

#include <iostream>
#include <list>

using namespace std;

class FiltroMedia : public Filtro{
	private:
		Cor **matriz;
		int altura;
		int largura;
	public:
		FiltroMedia();
		void setAltura(int altura);
		void setLargura(int largura);
		list<Cor> aplicaFiltro(list<Cor> cores, int maxEscala);
		void constroiMatriz(list<Cor> cores);
};
#endif
