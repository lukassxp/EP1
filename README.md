# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Descrição do projeto:

- O projeto consiste em um programa que aplica filtros e mascaras em imagens '.ppm', o funcionameto dele se da por meio da abertura e carregamento em um objeto do arquivo de imagem a que se deseja aplicar um filtro ou mascara, o objeto correga informações como cabeçalho e matriz de cores da imagem. A aplicação dos filtros e mascaras e feita atraves de alterações nas cores dos pixels da imagem que por sua vez correspondem cada um a um elemento da matriz é possivel também salvar as alterações em um novo arquivo criado a partir do objeto imagem. 

### Instruções de execução:

1 - Clonar para seu computador o repositorio ou baixar de forma compactada;

2 - Acessar o diretorio do projeto via terminal;

3 - Inserir o comando 'make clean' para limpar os arquivos objeto caso eles existam;

4 - Inserir o comando 'make' para compilar o programa;

5 - Inserir o comando 'make run' para iniciar o programa;

#### Condições para o correto funcionamento do programa:

1 - As imagens a que se deseja aplicar um filtro ou mascara devem estar dentro da pasta 'doc' que na pasta do projeto;

2 - As imagens devem ser da estenção '.ppm';

3 - No programa deve ser inserido apenas nome da imagem. Ex: 'flores.ppm' deve ser inserido como 'flores';

4 - Ao gravar uma nova imagem certifique-se que não existe outra imagem com o mesmo novo gravada na pasta 'doc';

##### Instruções para casos de excessão:

- O programa procura tratar o maior número de excessões possivel mas caso se depare com uma excessão favor teclar 'Ctrl c' e interromper a execução do programa. Caso tenha interrese em ajudar na correção de tal erro me reporte sobre ele ou se preferir tome a liberdade de inspecionar o código e caso tenha alguma sugestão de como resolver o problema entre em contato para podermos fazer os ajustes nessesarios e trocarmos conhecimento.  
