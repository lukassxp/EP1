#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <list>
#include <stdlib.h>
#include <stdio_ext.h>
#include "imagem.hpp"
#include "cor.hpp"
#include "filtro.hpp"
#include "filtroNegativo.hpp"
#include "filtroPretoBranco.hpp"
#include "filtroPolarizado.hpp"
#include "filtroMedia.hpp"

using namespace std;

void menuGeral(int *opcao){
	cout << "\nExercicio Pratico 1 - Menu: \n\n1 - Ler imagem\n2 - Aplicar filtro\n3 - Salvar imagem\n4 - Sair\n\n";
	
	do{
		cout << "Qual opcao deseja usar? ";
		__fpurge(stdin);
		scanf("%i",opcao);
		__fpurge(stdin);
		if(*opcao < 1 || *opcao > 4){
			cout << "\nOpcao invalida, deve ser um numero de 1 a 4!" << endl;
		}
	}while(*opcao < 1 || *opcao > 4);
}

void menuFiltro(int *opcao){
	cout << "\n2 - Aplicar Filtro - Menu: \n\n1 - Negativo\n2 - Preto e Branco\n3 - Polarizado\n4 - Media\n5 - Retornar ao menu inicial\n\n";
	
	do{
		cout << "Qual filtro deseja aplicar? ";
		__fpurge(stdin);
		scanf("%i",opcao);
		__fpurge(stdin);
		if(*opcao < 1 || *opcao > 5){
			cout << "\nOpcao invalida, deve ser um numero de 1 a 5!" << endl;
		}
	}while(*opcao < 1 || *opcao > 5);
}

int main(void){
	int opcao, option, flag = 0, aux = 0;

	ifstream arquivo;
	ofstream novoArquivo;

	FiltroMedia *filtroMedia;
	Filtro *filtro;

	Imagem *imagem;

	do{	
		cout << "|----------------------------------------------------------------|" << endl;
		menuGeral(&opcao);	

		switch(opcao){
			case 1: cout << "|----------------------------------------------------------------|" << endl;
				imagem = new Imagem();
				imagem->lerImagem(&arquivo);
				arquivo.close();
				flag = 1;
				aux = 0;
				break;

			case 2: if(flag == 1){
					cout << "|----------------------------------------------------------------|" << endl;
					menuFiltro(&option);
					switch(option){
						case 1: filtro = new FiltroNegativo();
							imagem->setCores(filtro->aplicaFiltro(imagem->getCores(), imagem->getMaxEscala()));
							aux = 1;
							break;

						case 2: filtro = new FiltroPretoBranco();
							imagem->setCores(filtro->aplicaFiltro(imagem->getCores(), imagem->getMaxEscala()));
							aux = 1;
							break;

						case 3: filtro = new FiltroPolarizado();
							imagem->setCores(filtro->aplicaFiltro(imagem->getCores(), imagem->getMaxEscala()));
							aux = 1;
							break;

						case 4: cout << "|----------------------------------------------------------------|" << endl;
							filtroMedia = new FiltroMedia();
							filtroMedia->setAltura(imagem->getAltura());
							filtroMedia->setLargura(imagem->getLargura());
							filtroMedia->constroiMatriz(imagem->getCores());	
							imagem->setCores(filtroMedia->aplicaFiltro(imagem->getCores(), imagem->getMaxEscala()));
							aux = 1;
							break;
					}
				}
				else{
					cout << "\nE preciso carregar uma imagem antes de aplicar um filtro!\n" << endl;
				}				
				break;
			case 3: if(flag == 1 && aux == 1){
					cout << "|----------------------------------------------------------------|" << endl;
					imagem->gravarImagem(&novoArquivo, &arquivo);
					arquivo.close();
					novoArquivo.close();
					aux = 0;
				}
				else{
					cout << "\nE preciso carregar uma imagem e aplicar filtro antes de acessar esse menu!\n" << endl;
				}
				break;	
		}
		cout << "|----------------------------------------------------------------|" << endl;
	}while(opcao != 4);

	return 0;
}
