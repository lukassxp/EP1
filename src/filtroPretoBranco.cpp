#include "filtroPretoBranco.hpp"

FiltroPretoBranco::FiltroPretoBranco(){

}

list<Cor> FiltroPretoBranco::aplicaFiltro(list<Cor> cores, int maxEscala){
	unsigned char aux = maxEscala;

	for(controlador = cores.begin(); controlador != cores.end(); controlador++){
		ajudante = *controlador;

		aux =(unsigned char)(0.299 * ajudante.getR()) + (0.587 * ajudante.getG()) + (0.144 * ajudante.getB());

		ajudante.setR(aux);
		ajudante.setG(aux);
		ajudante.setB(aux);

		pixels.push_back(ajudante);
	}
	cout << "\nFiltro aplicado com sucesso!\n" << endl;
	return pixels;
}
