#ifndef COR_HPP
#define COR_HPP

#include <iostream>

using namespace std;

class Cor {
	private:
		unsigned char r;
		unsigned char g;
		unsigned char b;
	public:
		Cor();
		Cor(unsigned char r, unsigned char g, unsigned char b);
		unsigned char getR();
		unsigned char getG();
		unsigned char getB();
		void setR(unsigned char r);
		void setG(unsigned char g);
		void setB(unsigned char b);
};
#endif
