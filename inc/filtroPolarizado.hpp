#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include "imagem.hpp"
#include "filtro.hpp"
#include "cor.hpp"

#include <iostream>
#include <list>

using namespace std;

class FiltroPolarizado : public Filtro{
	public:
		FiltroPolarizado();
		list<Cor> aplicaFiltro(list<Cor> cores, int maxEscala);
};
#endif

